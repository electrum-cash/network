import type { RPCError, RPCParameter, RPCResponse, RPCNotification } from './rpc-interfaces';
import type { EventEmitter } from 'eventemitter3';

/**
 * Optional settings that change the default behavior of the network connection.
 */
export interface ElectrumNetworkOptions
{
	/** If set to true, numbers that can safely be parsed as integers will be `BigInt` rather than `Number`. */
	useBigInt?: boolean;

	/** When connected, send a keep-alive Ping message this often. */
	sendKeepAliveIntervalInMilliSeconds?: number;

	/** When disconnected, attempt to reconnect after this amount of time. */
	reconnectAfterMilliSeconds?: number;

	/** After every send, verify that we have received data after this amount of time. */
	verifyConnectionTimeoutInMilliSeconds?: number;
}

/**
 * List of events emitted by the ElectrumSocket.
 * @event
 * @ignore
 */
export interface ElectrumSocketEvents
{
	/**
	 * Emitted when data has been received over the socket.
	 * @eventProperty
	 */
	'data': [ string ];

	/**
	 * Emitted when a socket connects.
	 * @eventProperty
	 */
	'connected': [];

	/**
	 * Emitted when a socket disconnects.
	 * @eventProperty
	 */
	'disconnected': [];

	/**
	 * Emitted when the socket has failed in some way.
	 * @eventProperty
	 */
	'error': [ Error ];
}

/**
 * Abstract socket used when communicating with Electrum servers.
 */
export interface ElectrumSocket extends EventEmitter<ElectrumSocketEvents>, ElectrumSocketEvents
{
	/**
	 * Utility function to provide a human accessible host identifier.
	 */
	get hostIdentifier(): string;

	/**
	 * Fully qualified domain name or IP address of the host
	 */
	host: string;

	/**
	 * Network port for the host to connect to, defaults to the standard TLS port
	 */
	port: number;

	/**
	 * If false, uses an unencrypted connection instead of the default on TLS
	 */
	encrypted: boolean;

	/**
	 * If no connection is established after `timeout` ms, the connection is terminated
	 */
	timeout: number;

	/**
	 * Connects to an Electrum server using the socket.
	 */
	connect(): void;

	/**
	 * Disconnects from the Electrum server from the socket.
	 */
	disconnect(): void;

	/**
	 * Write data to the Electrum server on the socket.
	 *
	 * @param data     - Data to be written to the socket
	 * @param callback - Callback function to be called when the write has completed
	 */
	write(data: Uint8Array | string, callback?: (err?: Error) => void): boolean;
}

/**
 * @ignore
 */
export interface VersionRejected
{
	error: RPCError;
}

/**
 * @ignore
 */
export interface VersionNegotiated
{
	software: string;
	protocol: string;
}

/**
 * @ignore
 */
export type VersionNegotiationResponse = VersionNegotiated | VersionRejected;

/**
 * List of events emitted by the ElectrumConnection.
 * @event
 * @ignore
 */
export interface ElectrumConnectionEvents
{
	/**
	 * Emitted when any data has been received over the network.
	 * @eventProperty
	 */
	'received': [];

	/**
	 * Emitted when a complete electrum message has been received over the network.
	 * @eventProperty
	 */
	'response': [ RPCResponse ];

	/**
	 * Emitted when the connection has completed version negotiation.
	 * @eventProperty
	 */
	'version': [ VersionNegotiationResponse ];

	/**
	 * Emitted when a network connection is initiated.
	 * @eventProperty
	 */
	'connecting': [];

	/**
	 * Emitted when a network connection is successful.
	 * @eventProperty
	 */
	'connected': [];

	/**
	 * Emitted when a network disconnection is initiated.
	 * @eventProperty
	 */
	'disconnecting': [];

	/**
	 * Emitted when a network disconnection is successful.
	 * @eventProperty
	 */
	'disconnected': [];

	/**
	 * Emitted when a network connect attempts to automatically reconnect.
	 * @eventProperty
	 */
	'reconnecting': [];

	/**
	 * Emitted when the network has failed in some way.
	 * @eventProperty
	 */
	'error': [ Error ];
}

/**
 * List of events emitted by the ElectrumClient.
 * @event
 * @ignore
 */
export interface ElectrumClientEvents
{
	/**
	 * Emitted when an electrum subscription statement has been received over the network.
	 * @eventProperty
	 */
	'notification': [ RPCNotification ];

	/**
	 * Emitted when a network connection is initiated.
	 * @eventProperty
	 */
	'connecting': [];

	/**
	 * Emitted when a network connection is successful.
	 * @eventProperty
	 */
	'connected': [];

	/**
	 * Emitted when a network disconnection is initiated.
	 * @eventProperty
	 */
	'disconnecting': [];

	/**
	 * Emitted when a network disconnection is successful.
	 * @eventProperty
	 */
	'disconnected': [];

	/**
	 * Emitted when a network connect attempts to automatically reconnect.
	 * @eventProperty
	 */
	'reconnecting': [];

	/**
	 * Emitted when the network has failed in some way.
	 * @eventProperty
	 */
	'error': [ Error ];
}

/**
 * A list of possible responses to requests.
 * @ignore
 */
export type RequestResponse = RPCParameter | RPCParameter[];

/**
 * Request resolvers are used to process the response of a request. This takes either
 * an error object or any stringified data, while the other parameter is omitted.
 * @ignore
 */
export type RequestResolver = (error?: Error, data?: string) => void;

/**
 * Typing for promise resolution.
 * @ignore
 */
export type ResolveFunction<T> = (value: T | PromiseLike<T>) => void;

/**
 * Typing for promise rejection.
 * @ignore
 */
export type RejectFunction = (reason?: any) => void;

/**
 * @ignore
 */
export const isVersionRejected = function(object: any): object is VersionRejected
{
	return 'error' in object;
};

/**
 * @ignore
 */
export const isVersionNegotiated = function(object: any): object is VersionNegotiated
{
	return 'software' in object && 'protocol' in object;
};
