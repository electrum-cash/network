import { RPCParameter } from './rpc-interfaces';

/**
 * Grouping of utilities that simplifies implementation of the Electrum protocol.
 *
 * @ignore
 */
export class ElectrumProtocol
{
	/**
	 * Helper function that builds an Electrum request object.
	 *
	 * @param method     - method to call.
	 * @param parameters - method parameters for the call.
	 * @param requestId  - unique string or number referencing this request.
	 *
	 * @returns a properly formatted Electrum request string.
	 */
	static buildRequestObject(method: string, parameters: RPCParameter[], requestId: string | number): string
	{
		// Return the formatted request object.
		// NOTE: Electrum either uses JsonRPC strictly or loosely.
		//       If we specify protocol identifier without being 100% compliant, we risk being disconnected/blacklisted.
		//       For this reason, we omit the protocol identifier to avoid issues.
		return JSON.stringify({ method: method, params: parameters, id: requestId });
	}

	/**
	 * Constant used to verify if a provided string is a valid version number.
	 *
	 * @returns a regular expression that matches valid version numbers.
	 */
	static get versionRegexp(): RegExp
	{
		return /^\d+(\.\d+)+$/;
	}

	/**
	 * Constant used to separate statements/messages in a stream of data.
	 *
	 * @returns the delimiter used by Electrum to separate statements.
	 */
	static get statementDelimiter(): string
	{
		return '\n';
	}
}
